
#property copyright ""
#property link      ""
#property version   "1.05"
#property description "csvfile_monitoring.ex4はスプレッド計測のCSVファイルを監視します"
#property description "日本時間9時1分に前日分のCSVファイルが存在するかを確認します"
#property strict

#define OPEN_TYPE_PRECONFIG                0           // use the configuration by default
#define FLAG_KEEP_CONNECTION               0x00400000  // do not terminate the connection
#define FLAG_PRAGMA_NOCACHE                0x00000100  // no cashing of the page
#define FLAG_RELOAD                        0x80000000  // receive the page from the server when accessing it
#define SERVICE_HTTP                       3           // the required protocol
#define DEFAULT_HTTPS_PORT                 443
#define FLAG_SECURE                        0x00800000  // use PCT/SSL if applicable (HTTP)
#define INTERNET_FLAG_SECURE               0x00800000
#define INTERNET_FLAG_KEEP_CONNECTION      0x00400000
#define HTTP_ADDREQ_FLAG_REPLACE           0x80000000
#define HTTP_ADDREQ_FLAG_ADD               0x20000000

#import "wininet.dll"
int InternetOpenW(string sAgent,int lAccessType,string sProxyName,string sProxyBypass,int lFlags);
int InternetConnectW(int hInternet,string ServerName, int nServerPort,string lpszUsername, string lpszPassword, int dwService,int dwFlags,int dwContext);
int HttpOpenRequestW(int hConnect, string Verb, string ObjectName, string Version, string Referer, string AcceptTypes, int dwFlags, int dwContext);
int HttpSendRequestW(int hRequest, string &lpszHeaders, int dwHeadersLength, uchar &lpOptional[], int dwOptionalLength);
int InternetReadFile(int hFile,uchar &sBuffer[],int lNumBytesToRead,int &lNumberOfBytesRead);
int InternetCloseHandle(int hInet);
#import

input string brockertypename = ""; //ブローカータイプ名(例:XMStandart)
input string name_ = "";           //計測者名
input int time_hour = 9;           //監視時間(時間)
input int time_min = 1;            //監視時間(分)
static datetime lastProcessedDate = 0;
bool ret_set_timer = false;

int OnInit()
{
  if(MQLInfoInteger(MQL_DLLS_ALLOWED) != 1) {
    Print("==================");
    Print("DLL is not allowed");
    Print("==================");
    Alert("DLL is not allowed");
    return(INIT_FAILED);
  }

  if(brockertypename==""){
    Alert("Please enter the broker type name.");
    return(INIT_FAILED);
  }
  if(name_==""){
    Alert("Please enter the confirmer name.");
    return(INIT_FAILED);
  }

  lastProcessedDate = 0;
  ret_set_timer = EventSetTimer(1);
  return(INIT_SUCCEEDED);
}


void OnDeinit(const int reason)
{
  EventKillTimer();
}


void OnTick()
{
}


void OnTimer()
{
  if(!ret_set_timer){
    Print("Timer not set. set timer now");
    ret_set_timer = EventSetTimer(1);
    return;
  }

  // ローカルコンピュータの現在時刻を取得 (※ローカルコンピュータが日本時間であること!!)
  datetime localTime = TimeLocal();

  // 初回実行時、または前回実行時の日付と現在の日付が同じ場合、処理をスキップ
  if(TimeDay(lastProcessedDate) == TimeDay(localTime)){
    return;
  }

  int dayOfWeek = TimeDayOfWeek(localTime);
  // 土曜日または日曜日の場合、処理をスキップ
  if(dayOfWeek==0 || dayOfWeek==6){
    return;
  }

  int hour = TimeHour(localTime);
  int minute = TimeMinute(localTime);
  //9時ではない、又は01分ではない場合、処理をスキップ
  if(hour!=time_hour || minute!=time_min){
    return;
  }

  // ファイル名の取得
  string filename = FindFileWithPattern();

  // ファイルが存在しないとき
  if(filename == ""){
    Loop_CallAppEngineAPI(brockertypename + ", ファイル無し, " + name_);
    lastProcessedDate = localTime;
    return;
  }

  // ファイルの中身の行数をカウント
  int file_count = CountLinesInFile(filename);
  if(file_count>=1441){
    Loop_CallAppEngineAPI(brockertypename + ", OK, " + name_);
  }else{
    Loop_CallAppEngineAPI(brockertypename + ", 欠損あり 行数：" + (string)file_count + "," + name_);
  }

  // 今日の日付を記録
  lastProcessedDate = localTime;
}


bool CallAppEngineAPI(string message){
  //+---------------------------------------------------+
  //|   Google AppEngineで定義したAPIを呼び出す関数
  //+---------------------------------------------------+

  string sServerName = "mcc-project-388710.an.r.appspot.com";
  string sObjectName = "/message";
  string headers = "Content-Type: application/json\r\nx-api-key: 12345abcdefgh";
  string dataString = "{\"message\": \"" + message +"\"}";
  int session = 0;
  int connect = 0;
  uchar data[];
  StringToCharArray(dataString, data, 0, -1, CP_UTF8);

  session = InternetOpenW("MetaTrader 4 Terminal", 0, "", "", 0);
  if(session <= 0){
    Print("-Error InternetOpenW , session : ", session);
    session=-1;
    return false;
  }

  connect = InternetConnectW(session, sServerName, DEFAULT_HTTPS_PORT, "", "", SERVICE_HTTP, 0, 0);
  if(connect <= 0){
    Closehandle(session, connect);
    Print("-Error InternetConnectW , connect : ", connect);
    return false;
  }

  int hRequest = HttpOpenRequestW(connect, "POST", sObjectName, "HTTP/1.1", NULL, NULL, (int)(FLAG_SECURE | FLAG_KEEP_CONNECTION | FLAG_RELOAD | FLAG_PRAGMA_NOCACHE), 0);
  if(hRequest <= 0){
    Closehandle(session, connect);
    InternetCloseHandle(hRequest);
    Print("-Error HttpOpenRequestW , hRequest : ", hRequest);
    return false;
  }

  int hSend = HttpSendRequestW(hRequest, headers, StringLen(headers), data, ArraySize(data) - 1);
  if(hSend <= 0) {
    Closehandle(session, connect);
    InternetCloseHandle(hSend);
    InternetCloseHandle(hRequest);
    Print("Error Sending Notification");
    return false;
  } else {
    Print("Notification Sent Successfully!");
  }

  InternetCloseHandle(hSend);
  InternetCloseHandle(hRequest);
  return true;
}

void Loop_CallAppEngineAPI(string text){
  //+---------------------------------------------------+
  //|   CallAppEngineAPI関数を呼び出す関数
  //+---------------------------------------------------+

  int apicallcounter = 0;
  while(apicallcounter < 5){
    bool api_ret = CallAppEngineAPI(text);
    if(api_ret){
      break;
    }else{
      apicallcounter++;
    }
  }
}

void Closehandle(int session_, int connect_){
  //+---------------------------------------------------+
  //|   セッションを閉じる関数
  //+---------------------------------------------------+

  if(session_ > 0){
    InternetCloseHandle(session_);
    session_=-1;
  }
  if(connect_ > 0) {
    InternetCloseHandle(connect_);
    connect_=-1;
  }
}

string FindFileWithPattern(){
  //+---------------------------------------------------+
  //|   指定したパターンに一致するファイルを検索する関数
  //+---------------------------------------------------+

  string fileName;
  datetime timelocal = TimeLocal();
  int year;
  int month;
  int day;

  int dayofweek = TimeDayOfWeek(timelocal); // ローカルPCの時間から曜日を取得(0：日曜日 ～ 6:土曜日)
  if(dayofweek == 1){
    //月曜日の場合、先週の金曜日の年、月、日を取得
    datetime targetday = timelocal - 24 * 60 * 60 * 3; // 先週の金曜日の日付を取得（24*3時間減算）
    year = TimeYear(targetday);
    month = TimeMonth(targetday);
    day = TimeDay(targetday);
  }else{
    //月曜日以外の場合、昨日の年、月、日を取得
    datetime targetday = timelocal - 24 * 60 * 60; // 前日の日付を取得（24時間減算）
    year = TimeYear(targetday);
    month = TimeMonth(targetday);
    day = TimeDay(targetday);
  }

  string dateStr = IntegerToString(year) + StringFormat("%02d", month) + StringFormat("%02d", day);
  string file_filter = "*-" + dateStr + ".csv";

  long handle = FileFindFirst(file_filter, fileName, 0);
  if(handle != INVALID_HANDLE){
    FileFindClose(handle);
    return fileName;
  }else{
    Print("handle is INVALID_HANDLE, maybe file not found");
    return "";
  }
}

int CountLinesInFile(string filename){
  //+---------------------------------------------------+
  //|   ファイルの中身の行数をカウントする関数
  //+---------------------------------------------------+

  int fileHandle = FileOpen(filename, FILE_READ, '\n'); // ファイルを開く

  // ファイルが正常に開かれたか確認
  if(fileHandle == INVALID_HANDLE){
    return -1;
  }

  int linesCount = 0; // 行数をカウントするための変数

  // ファイルの終わりに達するまで行を読み取る
  while(!FileIsEnding(fileHandle))
  {
    FileReadString(fileHandle);
    linesCount++;
  }

  // ファイルを閉じる
  FileClose(fileHandle);

  return linesCount;
}
