# check_mt4_csv_file_project


# 概要
本プロジェクトは  
MT4のFilesディレクトリ内のCSVファイルを日時で監視するものとなります。  
MT4では1分毎にスプレッドデータをCSVに出力しています。

# 監視の流れ
CSVファイルの監視は日本時間9時1分に実施されます。  
監視後、CSVファイルのステータスをPub/Subに送信します。  
CSVファイルのステータスは以下3つのステータスです。
- OK
- 欠損あり
- ファイル無し

Pub/Subに送信後、CloudFunctionsを呼び出して  
複数のMT4のcsvファイルのステータスをまとめて状況をDiscordに投稿します。  

![watch_flow](watch_flow.png)
