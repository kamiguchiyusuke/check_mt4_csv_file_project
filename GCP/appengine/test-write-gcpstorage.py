from google.cloud import storage

def upload_blob(bucket_name, source_file_name, destination_blob_name):
    """Uploads a file to the bucket and appends if the blob already exists."""
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)

    # Check if the blob already exists
    if blob.exists():
        # Read the existing data
        existing_data = blob.download_as_text()
        # Concatenate with the new data
        new_data = existing_data + '\n' + source_file_name
    else:
        new_data = source_file_name

    # Upload the new data
    blob.upload_from_string(new_data)

    print(f"File {source_file_name} uploaded to {destination_blob_name}.")

bucket_name = 'test-mcc-mt4-ea-measure-data'
source_file_name = 'Hello, World!'
destination_blob_name = 'destination-file-name'

upload_blob(bucket_name, source_file_name, destination_blob_name)
