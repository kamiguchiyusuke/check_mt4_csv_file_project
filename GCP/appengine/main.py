from flask import Flask, request, jsonify, abort
from google.cloud import pubsub_v1
from google.pubsub_v1.types import PullRequest
from google.pubsub_v1.types import AcknowledgeRequest
import datetime
import requests
import time

# ================== parameter==================
API_KEY = "apikey"
GCP_PROJECT_ID = "projectid"
GCP_SUBSCRIPTION_ID = "pussub_id"
BOT_TOKEN = 'discord-token' # DiscordのBotトークン
CHANNEL_ID = 'channel_id' # チャンネルID 


publisher = pubsub_v1.PublisherClient()
topic_path = publisher.topic_path(GCP_PROJECT_ID, 'mt4spread')

subscriber = pubsub_v1.SubscriberClient()
subscription_path = subscriber.subscription_path(GCP_PROJECT_ID, GCP_SUBSCRIPTION_ID)

app = Flask(__name__)

@app.route('/message', methods=['POST'])
def publish_message():
    # 受信データの取得
    api_key = request.headers.get('x-api-key')
    if api_key != API_KEY:
        abort(403, description="Unauthorized access")

    content = request.json
    message = content.get('message', 'No message provided')

    # テキストをbytes形式に変換
    data = message.encode('utf-8')
    print("data : ", data)

    # メッセージの発行を試行する回数
    max_retries = 3
    # それぞれの試行の間に待つ秒数
    retry_delay = 2

    # メッセージの発行を試行
    for retry in range(max_retries):
        try:
            # Pub/Subにメッセージを発行
            future = publisher.publish(topic_path, data)
            message_id = future.result() # ここでエラーが発生する可能性がある
            print(f"Message published with ID: {message_id}")
            break # 成功した場合、ループを抜ける
        except Exception as e:
            print(f"An error occurred: {e}")
            if retry < max_retries - 1:
                print(f"Retrying in {retry_delay} seconds...")
                time.sleep(retry_delay) # 指定された時間だけ待つ
            else:
                print("Max retries reached, giving up.")

    return jsonify({'status': 'message published'}), 200

if __name__ == '__main__':
    app.run(debug=True)
