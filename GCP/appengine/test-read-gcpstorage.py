from google.cloud import storage

BUCKET_NAME = 'buecket-name'
FILE_NAME = "filename"


def download_blob(bucket_name, blob_name):
    """Downloads a blob from the bucket."""
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(blob_name)

    content = blob.download_as_text()
    print(f"Content of {blob_name}:\n{content}")

download_blob(BUCKET_NAME, FILE_NAME)
