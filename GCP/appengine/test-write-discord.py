from google.cloud import storage
import requests


BUCKET_NAME = 'buecket-name'
FILE_NAME = "filename"


def post_to_discord(message):
    # DiscordのBotトークン
    BOT_TOKEN = 'bottoken'

    # チャンネルID
    CHANNEL_ID = 'CHANNEL_ID'

    # Discord APIのURL
    url = f"https://discord.com/api/channels/{CHANNEL_ID}/messages"

    # メッセージ内容
    payload = {
        "content": message
    }

    # ヘッダーに認証情報をセット
    headers = {
        "Authorization": f"Bot {BOT_TOKEN}",
        "Content-Type": "application/json"
    }

    # Discordにメッセージを投稿
    response = requests.post(url, json=payload, headers=headers)

    if response.status_code == 200:
        print("Message posted successfully")
    else:
        print("Failed to post message")
        print(response.json())

def text_fix(text):

    lines = text.strip().split("\n")

    # 行を1列目でソートするために、一時的なリストを作成
    sorting_list = [line.split(',') for line in lines]

    # 1列目でソート、大文字小文字を区別しない
    sorting_list.sort(key=lambda x: x[0].lower())

    # 出力文字列を作成
    output_text = "\n".join(["     ".join(row) for row in sorting_list])

    if "ファイル無し" in text:
        output_text = "@everyone\n```\n" + output_text +"\n```"
    else:
        output_text = "```\n" + output_text +"\n```"

    return output_text

def download_blob(bucket_name, blob_name):
    """Downloads a blob from the bucket."""
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(blob_name)

    content = blob.download_as_text()
    #print(f"Content of {blob_name}:\n{content}")
    return content

if __name__ == "__main__":

    text = download_blob(bucket_name, blob_name)
    message = text_fix(text)
    print(message)
    #post_to_discord(message)



