"""
概要:
このPythonスクリプトは、pubsubからデータを取得し
Discordにデータを投稿します。

詳細:
- MT4からスプレッドデータがファイルに生成されているかを確認して、AppEngine経由でPubSubに投入します。
- その投入データをCloudFunctionsを使って取得し、データを加工してDiscordに投稿します。

使用方法(例):
下記のコマンドはCloudShellから実行できます。
curl -X POST "https://us-central1-mcc-project-388710.cloudfunctions.net/send-spread-data-to-discord" \
-H "Authorization: Bearer $(gcloud auth print-identity-token)"


参考) PubSubにデータ投入するコマンド(コマンドプロンプトから実行できます。)
curl -X POST -H "x-api-key: api-key" -H "Content-Type: application/json" -d "{\"message\":\"test, OK, KING\"}" https://mcc-project-388710.an.r.appspot.com/message
注意点:

-------------------------------------
"""

from flask import Flask, request, jsonify, abort
from google.cloud import pubsub_v1
from google.pubsub_v1.types import PullRequest
from google.pubsub_v1.types import AcknowledgeRequest

import requests
import datetime

GCP_PROJECT_ID = "projectid"
GCP_SUBSCRIPTION_ID = "pussub_id"
BOT_TOKEN = 'discord-token' # DiscordのBotトークン
CHANNEL_ID = 'channel_id' # チャンネルID 

subscriber = pubsub_v1.SubscriberClient()
subscription_path = subscriber.subscription_path(GCP_PROJECT_ID, GCP_SUBSCRIPTION_ID)

app = Flask(__name__)

@app.route('/process_messages')
def process_messages(request):

    today = datetime.datetime.today().weekday()

    # 月曜は0、金曜は4。それ以外の曜日ならジョブをスキップ
    if today < 0 or today > 4:
        return 'Not executed, not a weekday', 200

    messages_text = ""

    while True:
        pull_request = PullRequest(subscription=subscription_path, max_messages=10)
        response = subscriber.pull(request=pull_request)

        # メッセージがない場合、ループを終了
        if not response.received_messages:
            print("No messages received.")
            break

        for received_message in response.received_messages:
            message = received_message.message.data.decode('utf-8')
            messages_text += message + '\n'  # 改行を追加してメッセージを連結

            ack_request = AcknowledgeRequest(subscription=subscription_path, ack_ids=[received_message.ack_id])
            subscriber.acknowledge(request=ack_request)

    # 最後の改行を削除
    if messages_text:
        messages_text = messages_text[:-1]
    message = text_fix(messages_text)

    #print(message) #debug用
    post_to_discord(message)

    return "Messages processed", 200


def text_fix(text):
    lines = text.strip().split("\n")

    # 行を1列目でソートするために、一時的なリストを作成
    sorting_list = [line.split(',') for line in lines]

    # 1列目でソート、大文字小文字を区別しない
    sorting_list.sort(key=lambda x: x[0].lower())

    # 出力文字列を作成
    output_text = "\n".join(["     ".join(row) for row in sorting_list])
    return output_text


def post_to_discord(text):
    message = text_fix(text)

    if "ファイル無し" in text:
        message =  f'@everyone\n```\n{message}\n```'
    else:
        message =  f'```\n{message}\n```'


    # メッセージ内容
    payload = {
        "content": message
    }

    # ヘッダーに認証情報をセット
    headers = {
        "Authorization": f"Bot {BOT_TOKEN}",
        "Content-Type": "application/json"
    }

    # Discord APIのURL
    url = f"https://discord.com/api/channels/{CHANNEL_ID}/messages"

    # Discordにメッセージを投稿
    response = requests.post(url, json=payload, headers=headers)

    if response.status_code == 200:
        print("Message posted successfully")
    else:
        print("Failed to post message")
        print(response.json())

if __name__ == '__main__':
    app.run(debug=True)
